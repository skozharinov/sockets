#include "app.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <iostream>
#include <netinet/in.h>

#include "exceptions.hpp"
#include "request.hpp"
#include "response.hpp"
#include "utils.hpp"

namespace sockets {
App::App() : AppMode(Mode::Help), OptionsDescription("Allowed options"), SocketFd(0) {
  // clang-format off
  OptionsDescription.add_options()
      ("help", "Print this message")
      ("download", "Download a file")
      ("upload", "Upload a file")
      ("local-path", boost::program_options::value<std::string>(), "Path to a local file")
      ("server-path", boost::program_options::value<std::string>(), "Path to a file on the server")
      ("server-host", boost::program_options::value<std::string>(), "IP of the server")
      ("server-port", boost::program_options::value<std::uint16_t>(), "Port on the server");
  // clang-format on
}

int App::operator()(int ArgCount, const char *ArgArr[]) noexcept {
  try {
    parseOptions(ArgCount, ArgArr);
  } catch (const OptionsException &e) {
    std::cerr << "Error while parsing arguments: " << e.what();
    return EXIT_FAILURE;
  }

  if (AppMode == Mode::Help) {
    std::cout << OptionsDescription;
    return EXIT_SUCCESS;
  }

  SocketFd = socket(PF_INET, SOCK_DGRAM, 0);

  if (SocketFd == -1) {
    char *ErrorMessage = strerror(errno);
    std::cerr << "Cannot create socket " << ErrorMessage;
    return EXIT_FAILURE;
  }

  sockaddr_in SockAddr{};

  SockAddr.sin_family = AF_INET;
  SockAddr.sin_addr.s_addr = ServerAddress;
  SockAddr.sin_port = ServerPort;

  int ProcedureExit = EXIT_FAILURE;

  switch (AppMode) {
  case Upload:
    ProcedureExit = upload();
    break;

  case Download:
    ProcedureExit = download();
    break;

  default:
    std::cerr << "This shouldn't happen";
    return EXIT_FAILURE;
  }

  close(SocketFd);

  return ProcedureExit;
}

void App::parseOptions(int ArgCount, const char *ArgArr[]) {
  using namespace boost::program_options;

  variables_map variables;

  try {
    store(parse_command_line(ArgCount, ArgArr, OptionsDescription), variables);
  } catch (const boost::program_options::error &e) {
    throw OptionsException(std::string("Failed to parse arguments: ") + e.what());
  }

  if (variables.count("help") > 0) {
    AppMode = Mode::Help;
  }

  if (variables.count("download") > 0 && variables.count("upload") > 0) {
    throw OptionsException("Cannot download and upload at the same time");
  }

  if (variables.count("download") > 0) {
    AppMode = Mode::Download;
  } else if (variables.count("upload") > 0) {
    AppMode = Mode::Upload;
  } else if (AppMode != Mode::Help) {
    throw OptionsException("Need to specify upload or download mode");
  }

  if (variables.count("local-path") > 0) {
    LocalPath = variables.at("local-path").as<std::string>();
  } else if (AppMode != Mode::Help) {
    throw OptionsException("No local path specified");
  }

  if (variables.count("server-path") > 0) {
    ServerPath = variables.at("server-path").as<std::string>();
  } else if (AppMode != Mode::Help) {
    throw OptionsException("No server path specified");
  }

  if (variables.count("server-host") > 0) {
    try {
      ServerAddress = utils::parseIPv4(variables.at("server-host").as<std::string>().c_str());
    } catch (ParsingException &e) {
      throw OptionsException(std::string("Invalid IPv4 address: ") + e.what());
    }
  } else if (AppMode != Mode::Help) {
    throw OptionsException("No address specified");
  }

  if (variables.count("server-port") > 0) {
    ServerPort = htons(variables.at("server-port").as<std::uint16_t>());

    if (ServerPort == 0) {
      throw OptionsException("Port cannot be 0");
    }
  } else if (AppMode != Mode::Help) {
    throw OptionsException("No port specified");
  }
}

int App::upload() noexcept {
  int Fd = open(LocalPath.c_str(), O_RDONLY);

  if (Fd < 0) {
    std::cerr << "Failed to open local file: " << strerror(errno);
    return EXIT_FAILURE;
  }

  sockaddr_in Addr{};

  Addr.sin_family = AF_INET;
  Addr.sin_addr.s_addr = ServerAddress;
  Addr.sin_port = ServerPort;

    if (connect(SocketFd, reinterpret_cast<sockaddr *>(&Addr), sizeof(Addr)) < 0) {
      std::cerr << "Failed to connect: " << strerror(errno);
      return EXIT_FAILURE;
    }

  DeleteRequest UserDeleteRequest(ServerPath);
  auto [UserDeleteRequestSize, UserDeleteRequestSerialized] = UserDeleteRequest.serialize();

  send(SocketFd, UserDeleteRequestSerialized.get(), UserDeleteRequestSize, 0);

  const std::size_t SERVER_RESPONSE_BUFFER_SIZE = 64 * 1024;

  auto ServerDeleteResponseBuffer = std::make_unique<std::byte[]>(SERVER_RESPONSE_BUFFER_SIZE);

  auto ServerDeleteResponseSize = recv(SocketFd, ServerDeleteResponseBuffer.get(), SERVER_RESPONSE_BUFFER_SIZE, 0);

  if (ServerDeleteResponseSize < 0) {
    std::cerr << "Failed to receive response: " << strerror(errno);
    close(Fd);
    return EXIT_FAILURE;
  }

  if (Response::getMethodOfSerialized(ServerDeleteResponseBuffer.get()) != Response::Method::Delete) {
    std::cerr << "Protocol error";
    close(Fd);
    return EXIT_FAILURE;
  }

  const std::uint16_t BUFFER_SIZE = 4096;

  std::uint64_t Offset = 0;

  while (true) {
    auto Buffer = std::make_unique<std::byte[]>(BUFFER_SIZE);
    auto BytesRead = read(Fd, Buffer.get(), BUFFER_SIZE);

    if (BytesRead == 0)
      break;

    PutRequest UserRequest(ServerPath, Offset, BytesRead, std::move(Buffer));

    auto [UserRequestSize, UserRequestSerialized] = UserRequest.serialize();

    send(SocketFd, UserRequestSerialized.get(), UserRequestSize, 0);

    auto ServerResponseBuffer = std::make_unique<std::byte[]>(SERVER_RESPONSE_BUFFER_SIZE);

    auto ServerResponseSize = recv(SocketFd, ServerResponseBuffer.get(), SERVER_RESPONSE_BUFFER_SIZE, 0);

    // TODO: Add retransmission
    // TODO: Do not wait for confirmation after each packet

    if (ServerResponseSize < 0) {
      std::cerr << "Failed to receive response: " << strerror(errno);
      close(Fd);
      return EXIT_FAILURE;
    }

    if (Response::getMethodOfSerialized(ServerResponseBuffer.get()) != Response::Method::Put) {
      std::cerr << "Protocol error";
      close(Fd);
      return EXIT_FAILURE;
    }

    auto ServerResponse = PutResponse::deserialize(ServerResponseBuffer.get());

    if (ServerResponse == nullptr) {
      std::cerr << "Protocol error";
      close(Fd);
      return EXIT_FAILURE;
    }

    if (ServerResponse->ResponseCode != Response::Code::Success) {
      std::cerr << "Server failed to accept file part";
      close(Fd);
      return EXIT_FAILURE;
    }

    Offset += BytesRead;
  }

  close(Fd);

  return EXIT_SUCCESS;
}

int App::download() noexcept {
  sockaddr_in Addr{};

  Addr.sin_family = AF_INET;
  Addr.sin_addr.s_addr = ServerAddress;
  Addr.sin_port = ServerPort;

  if (connect(SocketFd, reinterpret_cast<sockaddr *>(&Addr), sizeof(Addr)) < 0) {
    std::cerr << "Failed to connect: " << strerror(errno);
    return EXIT_FAILURE;
  }

  GetRequest UserRequest(ServerPath);

  auto [UserRequestSize, UserRequestSerialized] = UserRequest.serialize();

  send(SocketFd, UserRequestSerialized.get(), UserRequestSize, 0);

  const std::size_t SERVER_RESPONSE_BUFFER_SIZE = 64 * 1024;

  auto ServerFirstResponseBuffer = std::make_unique<std::byte[]>(SERVER_RESPONSE_BUFFER_SIZE);

  auto ServerFirstResponseSize = recv(SocketFd, ServerFirstResponseBuffer.get(), SERVER_RESPONSE_BUFFER_SIZE, 0);

  if (ServerFirstResponseSize < 0) {
    std::cerr << "Failed to receive response: " << strerror(errno);
    return EXIT_FAILURE;
  }

  if (Response::getMethodOfSerialized(ServerFirstResponseBuffer.get()) != Response::Method::Get) {
    std::cerr << "Protocol error";
    return EXIT_FAILURE;
  }

  auto ServerFirstResponse = GetResponse::deserialize(ServerFirstResponseBuffer.get());

  if (ServerFirstResponse == nullptr) {
    std::cerr << "Protocol error";
    return EXIT_FAILURE;
  }

  if (ServerFirstResponse->ResponseCode == Response::Code::FileNotFound) {
    std::cerr << "File not found on server";
    return EXIT_FAILURE;
  }

  if (ServerFirstResponse->ResponseCode != Response::Code::Success) {
    std::cerr << "Server failed to send file";
    return EXIT_FAILURE;
  }

  int Fd = creat(LocalPath.c_str(), 00755);

  if (Fd < 0) {
    std::cerr << "Failed to open local file: " << strerror(errno);
    return EXIT_FAILURE;
  }

  lseek(Fd, ServerFirstResponse->Offset, SEEK_SET);
  write(Fd, ServerFirstResponse->Data.get(), ServerFirstResponse->Length);

  for (std::uint64_t i = 0; i < ServerFirstResponse->ChunksNumber - 1; ++i) {
    auto ServerResponseBuffer = std::make_unique<std::byte[]>(SERVER_RESPONSE_BUFFER_SIZE);

    auto ServerResponseSize = recv(SocketFd, ServerResponseBuffer.get(), SERVER_RESPONSE_BUFFER_SIZE, 0);

    if (ServerResponseSize < 0) {
      std::cerr << "Failed to receive response: " << strerror(errno);
      close(Fd);
      return EXIT_FAILURE;
    }

    if (Response::getMethodOfSerialized(ServerResponseBuffer.get()) != Response::Method::Get) {
      std::cerr << "Protocol error";
      close(Fd);
      return EXIT_FAILURE;
    }

    auto ServerResponse = GetResponse::deserialize(ServerResponseBuffer.get());

    if (ServerResponse == nullptr) {
      std::cerr << "Protocol error";
      close(Fd);
      return EXIT_FAILURE;
    }

    if (ServerResponse->ResponseCode != Response::Code::Success) {
      std::cerr << "Server failed to send file";
      close(Fd);
      return EXIT_FAILURE;
    }

    lseek(Fd, ServerResponse->Offset, SEEK_SET);
    write(Fd, ServerResponse->Data.get(), ServerResponse->Length);
  }

  close(Fd);

  return EXIT_SUCCESS;
}
} // namespace sockets
