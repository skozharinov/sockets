#include "app.hpp"

int main(int argc, const char *argv[]) {
  sockets::App app;
  return app(argc, argv);
}
