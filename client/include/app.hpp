#ifndef SOCKETS_APP_HPP
#define SOCKETS_APP_HPP

#include <string>

#include <boost/program_options.hpp>

namespace sockets {
class App {
public:
  enum Mode { Help = 0, Upload = 1, Download = 2 };

  App();

  int operator()(int ArgCount, const char *ArgArr[]) noexcept;

protected:
  void parseOptions(int ArgCount, const char *ArgArr[]);

  int upload() noexcept;
  int download() noexcept;

private:
  Mode AppMode;
  std::string LocalPath, ServerPath;
  std::uint32_t ServerAddress;
  std::uint16_t ServerPort;
  boost::program_options::options_description OptionsDescription;
  int SocketFd;
};
} // namespace sockets

#endif // SOCKETS_APP_HPP
