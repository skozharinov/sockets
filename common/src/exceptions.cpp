#include "exceptions.hpp"

namespace sockets {
OptionsException::OptionsException(std::string Cause) : Cause(std::move(Cause)) {}

const char *OptionsException::what() const noexcept { return Cause.c_str(); }

ParsingException::ParsingException(std::string Cause) : Cause(std::move(Cause)) {}

const char *ParsingException::what() const noexcept { return Cause.c_str(); }
} // namespace sockets
