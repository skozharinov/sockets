#include "utils.hpp"

#include <cctype>
#include <netinet/in.h>

#include "exceptions.hpp"

namespace sockets::utils {
std::uint32_t parseIPv4(const char *Address) {
  std::uint8_t Components[4]{0, 0, 0, 0};

  std::size_t Index = 0;

  while (Index < 4) {
    if (std::isdigit(Address[0])) {
      if (__builtin_mul_overflow(Components[Index], 10, &Components[Index]))
        throw ParsingException("Component is greater than 255");

      if (__builtin_add_overflow(Components[Index], Address[0] - '0', &Components[Index]))
        throw ParsingException("Component is greater than 255");
    } else if (Address[0] == '.') {
      ++Index;
    } else if (Address[0] == '\0' && Index == 3) {
      ++Index;
      break;
    } else if (Address[0] == '\0' && Index < 3) {
      throw ParsingException("Less than 4 components specified");
    } else {
      throw ParsingException("Unexpected character");
    }

    ++Address;
  }

  if (Address[0] != '\0')
    throw ParsingException("More that 4 components specified");

  return htonl((Components[0] << 24) | (Components[1] << 16) | (Components[2] << 8) | Components[3]);
}

std::string printIPv4(std::uint32_t Address) {
  Address = ntohl(Address);
  std::uint8_t Components[]{static_cast<std::uint8_t>(Address >> 24), static_cast<std::uint8_t>(Address >> 16),
                            static_cast<std::uint8_t>(Address >> 8), static_cast<std::uint8_t>(Address)};

  char Buffer[15];

  std::sprintf(Buffer, "%u.%u.%u.%u", Components[0], Components[1], Components[2], Components[3]);

  return {Buffer};
}
} // namespace sockets::utils
