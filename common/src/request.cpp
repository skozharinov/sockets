#include "request.hpp"

#include <cstring>

namespace sockets {

Request::Request(Method RequestMethod, std::string Filename)
    : RequestMethod(RequestMethod), Filename(std::move(Filename)) {}

Request::Method Request::getMethodOfSerialized(std::byte *Data) { return static_cast<Method>(Data[0]); }

GetRequest::GetRequest(std::string Filename) : Request(Method::Get, std::move(Filename)) {}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> GetRequest::serialize() noexcept {
  std::uint16_t FilenameLength = Filename.length();

  std::size_t TotalLength = 1 + sizeof(FilenameLength) + FilenameLength;
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(RequestMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&FilenameLength), sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  std::memcpy(SerializedData.get() + Index, Filename.data(), FilenameLength);
  Index += FilenameLength;

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<GetRequest> GetRequest::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Get) {
    return nullptr;
  }

  std::uint16_t FilenameLength;
  std::memcpy(reinterpret_cast<std::byte *>(&FilenameLength), SerializedData + Index, sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  auto Filename = std::make_unique<std::byte[]>(FilenameLength);
  std::memcpy(Filename.get(), SerializedData + Index, FilenameLength);
  Index += FilenameLength;

  return std::make_unique<GetRequest>(std::string(reinterpret_cast<char *>(Filename.get()), FilenameLength));
}

PutRequest::PutRequest(std::string Filename, std::uint64_t Offset, std::uint16_t Length,
                       std::unique_ptr<std::byte[]> Data)
    : Request(Method::Put, std::move(Filename)), Offset(Offset), Length(Length), Data(std::move(Data)) {}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> PutRequest::serialize() noexcept {
  std::uint16_t FilenameLength = Filename.length();

  std::size_t TotalLength = 1 + sizeof(FilenameLength) + FilenameLength + sizeof(Offset) + sizeof(Length) + Length;
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(RequestMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&FilenameLength), sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  std::memcpy(SerializedData.get() + Index, Filename.data(), FilenameLength);
  Index += FilenameLength;

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Offset), sizeof(Offset));
  Index += sizeof(Offset);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Length), sizeof(Length));
  Index += sizeof(Length);

  std::memcpy(SerializedData.get() + Index, Data.get(), Length);
  Index += Length;

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<PutRequest> PutRequest::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Put) {
    return nullptr;
  }

  std::uint16_t FilenameLength;
  std::memcpy(reinterpret_cast<std::byte *>(&FilenameLength), SerializedData + Index, sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  auto Filename = std::make_unique<std::byte[]>(FilenameLength);
  std::memcpy(Filename.get(), SerializedData + Index, FilenameLength);
  Index += FilenameLength;

  std::uint64_t Offset;
  std::memcpy(reinterpret_cast<std::byte *>(&Offset), SerializedData + Index, sizeof(Offset));
  Index += sizeof(Offset);

  std::uint16_t Length;
  std::memcpy(reinterpret_cast<std::byte *>(&Length), SerializedData + Index, sizeof(Length));
  Index += sizeof(Length);

  auto Data = std::make_unique<std::byte[]>(Length);
  std::memcpy(Data.get(), SerializedData + Index, Length);
  Index += Length;

  return std::make_unique<PutRequest>(std::string(reinterpret_cast<char *>(Filename.get()), FilenameLength), Offset,
                                      Length, std::move(Data));
}

DeleteRequest::DeleteRequest(std::string Filename) : Request(Method::Delete, std::move(Filename)) {

}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> DeleteRequest::serialize() noexcept {
  std::uint16_t FilenameLength = Filename.length();

  std::size_t TotalLength = 1 + sizeof(FilenameLength) + FilenameLength;
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(RequestMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&FilenameLength), sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  std::memcpy(SerializedData.get() + Index, Filename.data(), FilenameLength);
  Index += FilenameLength;

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<DeleteRequest> DeleteRequest::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Delete) {
    return nullptr;
  }

  std::uint16_t FilenameLength;
  std::memcpy(reinterpret_cast<std::byte *>(&FilenameLength), SerializedData + Index, sizeof(FilenameLength));
  Index += sizeof(FilenameLength);

  auto Filename = std::make_unique<std::byte[]>(FilenameLength);
  std::memcpy(Filename.get(), SerializedData + Index, FilenameLength);
  Index += FilenameLength;

  return std::make_unique<DeleteRequest>(std::string(reinterpret_cast<char *>(Filename.get()), FilenameLength));
}
} // namespace sockets
