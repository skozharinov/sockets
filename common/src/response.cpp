#include "response.hpp"

#include <cstring>

namespace sockets {
Response::Response(Method ResponseMethod, Code ResponseCode)
    : ResponseMethod(ResponseMethod), ResponseCode(ResponseCode) {}

Response::Method Response::getMethodOfSerialized(std::byte *Data) { return static_cast<Method>(Data[0]); }

GetResponse::GetResponse(Code ResponseCode, std::uint64_t ChunksNumber, std::uint64_t Offset, std::uint16_t Length,
                         std::unique_ptr<std::byte[]> Data)
    : Response(Method::Get, ResponseCode), ChunksNumber(ChunksNumber), Offset(Offset), Length(Length), Data(std::move(Data)) {}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> GetResponse::serialize() noexcept {
  std::size_t TotalLength = 1 + sizeof(ResponseCode) + sizeof(ChunksNumber) + sizeof(Offset) + sizeof(Length) + Length;
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(ResponseMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&ResponseCode), sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&ChunksNumber), sizeof(ChunksNumber));
  Index += sizeof(ChunksNumber);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Offset), sizeof(Offset));
  Index += sizeof(Offset);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Length), sizeof(Length));
  Index += sizeof(Length);

  std::memcpy(SerializedData.get() + Index, Data.get(), Length);
  Index += Length;

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<GetResponse> GetResponse::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Get) {
    return nullptr;
  }

  // TODO: Handle wrong enum values
  Code ResponseCode;
  std::memcpy(reinterpret_cast<std::byte *>(&ResponseCode), SerializedData + Index, sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  std::uint64_t ChunksNumber;
  std::memcpy(reinterpret_cast<std::byte *>(&ChunksNumber), SerializedData + Index, sizeof(ChunksNumber));
  Index += sizeof(ChunksNumber);

  std::uint64_t Offset;
  std::memcpy(reinterpret_cast<std::byte *>(&Offset), SerializedData + Index, sizeof(Offset));
  Index += sizeof(Offset);

  std::uint16_t Length;
  std::memcpy(reinterpret_cast<std::byte *>(&Length), SerializedData + Index, sizeof(Length));
  Index += sizeof(Length);

  auto Data = std::make_unique<std::byte[]>(Length);
  std::memcpy(Data.get(), SerializedData + Index, Length);
  Index += Length;

  return std::make_unique<GetResponse>(ResponseCode, ChunksNumber, Offset, Length, std::move(Data));
}

PutResponse::PutResponse(Code ResponseCode, std::uint64_t Offset, std::uint16_t Length)
    : Response(Method::Put, ResponseCode), Offset(Offset), Length(Length) {}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> PutResponse::serialize() noexcept {
  std::size_t TotalLength = 1 + sizeof(ResponseCode) + sizeof(Offset) + sizeof(Length);
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(ResponseMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&ResponseCode), sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Offset), sizeof(Offset));
  Index += sizeof(Offset);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&Length), sizeof(Length));
  Index += sizeof(Length);

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<PutResponse> PutResponse::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Put) {
    return nullptr;
  }

  // TODO: Handle wrong enum values
  Code ResponseCode;
  std::memcpy(reinterpret_cast<std::byte *>(&ResponseCode), SerializedData + Index, sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  std::uint64_t Offset;
  std::memcpy(reinterpret_cast<std::byte *>(&Offset), SerializedData + Index, sizeof(Offset));
  Index += sizeof(Offset);

  std::uint8_t Length;
  std::memcpy(reinterpret_cast<std::byte *>(&Length), SerializedData + Index, sizeof(Length));
  Index += sizeof(Length);

  return std::make_unique<PutResponse>(ResponseCode, Offset, Length);
}

DeleteResponse::DeleteResponse(Code ResponseCode) : Response(Method::Delete, ResponseCode) {}

std::pair<std::size_t, std::unique_ptr<std::byte[]>> DeleteResponse::serialize() noexcept {
  std::size_t TotalLength = 1 + sizeof(ResponseCode);
  auto SerializedData = std::make_unique<std::byte[]>(TotalLength);

  std::size_t Index = 0;

  SerializedData[Index++] = static_cast<std::byte>(ResponseMethod);

  std::memcpy(SerializedData.get() + Index, reinterpret_cast<std::byte *>(&ResponseCode), sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  return {TotalLength, std::move(SerializedData)};
}

std::unique_ptr<DeleteResponse> DeleteResponse::deserialize(const std::byte *SerializedData) noexcept {
  // TODO: Handle out-of-bounds
  std::size_t Index = 0;

  auto ResponseMethod = static_cast<Method>(SerializedData[Index++]);

  if (ResponseMethod != Delete) {
    return nullptr;
  }

  // TODO: Handle wrong enum values
  Code ResponseCode;
  std::memcpy(reinterpret_cast<std::byte *>(&ResponseCode), SerializedData + Index, sizeof(ResponseCode));
  Index += sizeof(ResponseCode);

  return std::make_unique<DeleteResponse>(ResponseCode);
}
} // namespace sockets
