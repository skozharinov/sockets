#ifndef SOCKETS_RESPONSE_HPP
#define SOCKETS_RESPONSE_HPP

#include <cstdint>
#include <memory>
#include <string>

namespace sockets {
class Response {
public:
  enum Method : std::uint8_t { Get = 1, Put = 2, Delete = 3 };
  enum Code : std::uint8_t { Success = 1, FileNotFound = 2, Unknown = 100 };

  explicit Response(Method ResponseMethod, Code ResponseCode);

  static Method getMethodOfSerialized(std::byte *Data);

  // TODO: Hide class members
  Method ResponseMethod;
  Code ResponseCode;
};

class GetResponse : public Response {
public:
  GetResponse(Code ResponseCode, std::uint64_t ChunksNumber, std::uint64_t Offset, std::uint16_t Length, std::unique_ptr<std::byte[]> Data);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<GetResponse> deserialize(const std::byte *SerializedData) noexcept;

  // TODO: Hide class members
  std::uint64_t ChunksNumber;
  std::uint64_t Offset;
  std::uint16_t Length;
  std::unique_ptr<std::byte[]> Data;
};

class PutResponse : public Response {
public:
  explicit PutResponse(Code ResponseCode, std::uint64_t Offset, std::uint16_t Length);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<PutResponse> deserialize(const std::byte *SerializedData) noexcept;

  // TODO: Hide class members
  std::uint64_t Offset;
  std::uint16_t Length;
};

class DeleteResponse : public Response {
public:
  explicit DeleteResponse(Code ResponseCode);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<DeleteResponse> deserialize(const std::byte *SerializedData) noexcept;
};
} // namespace sockets

#endif // SOCKETS_RESPONSE_HPP
