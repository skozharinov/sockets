#ifndef SOCKETS_UTILS_HPP
#define SOCKETS_UTILS_HPP

#include <cstdint>
#include <string>

namespace sockets::utils {
std::uint32_t parseIPv4(const char *Address);

std::string printIPv4(std::uint32_t Address);
}

#endif // SOCKETS_UTILS_HPP
