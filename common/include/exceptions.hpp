#ifndef SOCKETS_EXCEPTIONS_HPP
#define SOCKETS_EXCEPTIONS_HPP

#include <exception>
#include <string>

namespace sockets {
class OptionsException : public std::exception {
public:
  explicit OptionsException(std::string Cause);

  [[nodiscard]] const char *what() const noexcept override;

private:
  const std::string Cause;
};

class ParsingException : public std::exception {
public:
  explicit ParsingException(std::string Cause);

  [[nodiscard]] const char *what() const noexcept override;

private:
  const std::string Cause;
};
} // namespace sockets

#endif // SOCKETS_EXCEPTIONS_HPP
