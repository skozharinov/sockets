#ifndef SOCKETS_REQUEST_HPP
#define SOCKETS_REQUEST_HPP

#include <cstdint>
#include <memory>
#include <string>

namespace sockets {
class Request {
public:
  enum Method { Get = 1, Put = 2, Delete = 3 };

  Request(Method RequestMethod, std::string Filename);

  static Method getMethodOfSerialized(std::byte *Data);

  // TODO: Hide class members
  Method RequestMethod;
  std::string Filename;
};

class GetRequest : public Request {
public:
  explicit GetRequest(std::string Filename);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<GetRequest> deserialize(const std::byte *SerializedData) noexcept;
};

class PutRequest : public Request {
public:
  PutRequest(std::string Filename, std::uint64_t Offset, std::uint16_t Length, std::unique_ptr<std::byte[]> Data);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<PutRequest> deserialize(const std::byte *SerializedData) noexcept;

  // TODO: Hide class members
  std::uint64_t Offset;
  std::uint16_t Length;
  std::unique_ptr<std::byte[]> Data;
};

class DeleteRequest : public Request {
public:
  explicit DeleteRequest(std::string Filename);

  std::pair<std::size_t, std::unique_ptr<std::byte[]>> serialize() noexcept;

  static std::unique_ptr<DeleteRequest> deserialize(const std::byte *SerializedData) noexcept;
};
} // namespace sockets

#endif // SOCKETS_REQUEST_HPP
