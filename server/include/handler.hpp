#ifndef SOCKETS_HEADER_HPP
#define SOCKETS_HEADER_HPP

#include <condition_variable>
#include <cstdint>
#include <mutex>
#include <netinet/in.h>
#include <queue>

namespace sockets {
class GetRequest;
class GetResponse;
class PutRequest;
class PutResponse;
class DeleteRequest;
class DeleteResponse;

class Handler {
public:
  using QueueEntry = std::tuple<sockaddr_in, std::size_t, std::unique_ptr<std::byte[]>>;

  Handler(std::string RootPath, std::queue<QueueEntry> &DataQueue, std::mutex &DataMutex,
          std::condition_variable &DataCV, std::queue<QueueEntry> &ResponseQueue, std::mutex &ResponseMutex);

  void operator()() noexcept;

protected:
  void loop() noexcept;

  std::vector<GetResponse> handleGetRequest(GetRequest &&UserRequest) noexcept;
  PutResponse handlePutRequest(PutRequest &&UserRequest) noexcept;
  DeleteResponse handleDeleteRequest(DeleteRequest &&UserRequest) noexcept;

private:
  const std::string RootPath;
  std::queue<QueueEntry> &DataQueue;
  std::mutex &DataMutex;
  std::condition_variable &DataCV;
  std::queue<QueueEntry> &ResponseQueue;
  std::mutex &ResponseMutex;
};
} // namespace sockets

#endif // SOCKETS_HEADER_HPP
