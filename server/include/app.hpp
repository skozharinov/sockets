#ifndef SOCKETS_APP_HPP
#define SOCKETS_APP_HPP

#include <queue>
#include <mutex>
#include <vector>
#include <thread>
#include <memory>
#include <condition_variable>
#include <netinet/in.h>

#include <boost/program_options.hpp>

namespace sockets {
class App {
public:
  using QueueEntry = std::tuple<sockaddr_in, std::size_t, std::unique_ptr<std::byte[]>>;

  App();

  int operator()(int ArgCount, const char *ArgArr[]) noexcept;

protected:
  void parseOptions(int ArgCount, const char *ArgArr[]);

  void loop() noexcept;

  void handleIncoming() noexcept;
  void handleOutgoing() noexcept;


private:
  std::uint32_t ListenAddress;
  std::uint16_t ListenPort;
  std::string StoragePath;
  boost::program_options::options_description OptionsDescription;
  bool IsHelpRequested;
  int SocketFd;
  std::queue<QueueEntry> DataQueue;
  std::mutex DataMutex;
  std::condition_variable DataCV;
  std::queue<QueueEntry> ResponseQueue;
  std::mutex ResponseMutex;
  std::vector<std::thread> Threads;
};
} // namespace sockets

#endif // SOCKETS_APP_HPP
