#include "app.hpp"

int main(int argc, const char *argv[]) {
  sockets::App App;
  return App(argc, argv);
}
