#include "app.hpp"

#include <cerrno>
#include <iostream>
#include <netinet/in.h>
#include <sys/socket.h>

#include "exceptions.hpp"
#include "handler.hpp"
#include "utils.hpp"

namespace sockets {
App::App() : ListenAddress(0), ListenPort(0), OptionsDescription("Allowed options"), IsHelpRequested(false), SocketFd(0) {
  // clang-format off
  OptionsDescription.add_options()
      ("help", "Print this message")
      ("listen-host", boost::program_options::value<std::string>(), "IP to listen to")
      ("listen-port", boost::program_options::value<std::uint16_t>(), "Port to listen to")
      ("storage-path", boost::program_options::value<std::string>(), "Path to a folder with files");
  // clang-format on
}

int App::operator()(int ArgCount, const char *ArgArr[]) noexcept {
  try {
    parseOptions(ArgCount, ArgArr);
  } catch (const OptionsException &e) {
    std::cerr << "Error while parsing arguments: " << e.what();
    return EXIT_FAILURE;
  }

  if (IsHelpRequested) {
    std::cout << OptionsDescription;
    return EXIT_SUCCESS;
  }

  // TODO: Remove debug message
  std::cout << ListenAddress << " " << ListenPort << " " << StoragePath << "\n";

  SocketFd = socket(PF_INET, SOCK_DGRAM, 0);

  if (SocketFd == -1) {
    char *ErrorMessage = strerror(errno);
    std::cerr << "Cannot create socket " << ErrorMessage;
    return EXIT_FAILURE;
  }

  sockaddr_in SockAddr{};

  SockAddr.sin_family = AF_INET;
  SockAddr.sin_addr.s_addr = ListenAddress;
  SockAddr.sin_port = ListenPort;

  if (bind(SocketFd, reinterpret_cast<sockaddr *>(&SockAddr), sizeof(SockAddr)) < 0) {
    char *ErrorMessage = strerror(errno);
    std::cerr << "Cannot bind: " << ErrorMessage;
    return EXIT_FAILURE;
  }

  const std::size_t NUM_THREADS = 32;

  Threads.reserve(NUM_THREADS);

  for (std::size_t i = 0; i < NUM_THREADS; ++i) {
    Handler Handler(StoragePath, DataQueue, DataMutex, DataCV, ResponseQueue, ResponseMutex);
    Threads.emplace_back(Handler);
  }

  while (true) {
    loop();
  }

  if (close(SocketFd) < 0) {
    char *ErrorMessage = strerror(errno);
    std::cerr << "Cannot close the socket: " << ErrorMessage;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}

void App::parseOptions(int ArgCount, const char *ArgArr[]) {
  using namespace boost::program_options;

  variables_map variables;

  try {
    store(parse_command_line(ArgCount, ArgArr, OptionsDescription), variables);
  } catch (const boost::program_options::error &e) {
    throw OptionsException(std::string("Failed to parse arguments: ") + e.what());
  }

  if (variables.count("help") > 0) {
    IsHelpRequested = true;
  }

  if (variables.count("listen-host") > 0) {
    try {
      ListenAddress = utils::parseIPv4(variables.at("listen-host").as<std::string>().c_str());
    } catch (ParsingException &e) {
      throw OptionsException(std::string("Invalid IPv4 address: ") + e.what());
    }
  } else if (!IsHelpRequested) {
    throw OptionsException("No address specified");
  }

  if (variables.count("listen-port") > 0) {
    ListenPort = htons(variables.at("listen-port").as<std::uint16_t>());

    if (ListenPort == 0) {
      throw OptionsException("Port cannot be 0");
    }
  } else if (!IsHelpRequested) {
    throw OptionsException("No port specified");
  }

  if (variables.count("storage-path") > 0) {
    StoragePath = variables.at("storage-path").as<std::string>();
  } else if (!IsHelpRequested) {
    throw OptionsException("No storage path specified");
  }
}

void App::loop() noexcept {
  handleIncoming();
  handleOutgoing();
  std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

void App::handleIncoming() noexcept {
  const std::size_t BUFFER_SIZE = 64 * 1024;

  auto Buffer = std::make_unique<std::byte[]>(BUFFER_SIZE);

  sockaddr_in SourceAddr{};
  socklen_t SourceAddrLength = sizeof(SourceAddr);

  auto ReceivedSize = recvfrom(SocketFd, Buffer.get(), BUFFER_SIZE, MSG_DONTWAIT,
                               reinterpret_cast<sockaddr *>(&SourceAddr), &SourceAddrLength);

  if (ReceivedSize < 0) {
    if (errno == EWOULDBLOCK)
      return;

    char *ErrorMessage = strerror(errno);
    std::cerr << "Cannot receive from socket: " << ErrorMessage;
    return;
  }

  {
    std::unique_lock Lock(DataMutex);
    DataQueue.emplace(SourceAddr, ReceivedSize, std::move(Buffer));
  }

  DataCV.notify_one();
}

void App::handleOutgoing() noexcept {
  QueueEntry ServerResponse;

  {
    std::unique_lock Lock(ResponseMutex);

    if (ResponseQueue.empty())
      return;

    ServerResponse = std::move(ResponseQueue.front());
    ResponseQueue.pop();
  }

  auto [DestinationAddress, ServerResponseSize, ServerResponseData] = std::move(ServerResponse);

  sendto(SocketFd, ServerResponseData.get(), ServerResponseSize, 0, reinterpret_cast<sockaddr *>(&DestinationAddress),
         sizeof(DestinationAddress));
}
} // namespace sockets
