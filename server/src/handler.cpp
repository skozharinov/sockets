#include "handler.hpp"

#include <fcntl.h>
#include <unistd.h>
#include <filesystem>
#include <iostream>

#include "request.hpp"
#include "response.hpp"
#include "utils.hpp"

namespace sockets {
Handler::Handler(std::string RootPath, std::queue<QueueEntry> &DataQueue, std::mutex &DataMutex,
                 std::condition_variable &DataCV, std::queue<QueueEntry> &ResponseQueue, std::mutex &ResponseMutex)
    : RootPath(std::move(RootPath)), DataQueue(DataQueue), DataMutex(DataMutex), DataCV(DataCV),
      ResponseQueue(ResponseQueue), ResponseMutex(ResponseMutex) {}

void Handler::operator()() noexcept {
  while (true) {
    loop();
  }
}

void Handler::loop() noexcept {
  sockaddr_in Addr{};
  std::size_t Size{};
  std::unique_ptr<std::byte[]> Data;

  {
    std::unique_lock Lock(DataMutex, std::defer_lock);

    DataCV.wait(Lock, [&]() { return !DataQueue.empty(); });

    std::tie(Addr, Size, Data) = std::move(DataQueue.front());
    DataQueue.pop();
  }

  switch (Request::getMethodOfSerialized(Data.get())) {
  case Request::Method::Get: {
    std::cout << "Received GET request from " << utils::printIPv4(Addr.sin_addr.s_addr) << ":" << ntohs(Addr.sin_port)
              << "\n";

    auto UserRequest = GetRequest::deserialize(Data.get());
    auto ServerResponses = handleGetRequest(std::move(*UserRequest));

    std::vector<std::pair<std::size_t, std::unique_ptr<std::byte[]>>> SerializedResponses;
    SerializedResponses.reserve(ServerResponses.size());

    for (auto &ServerResponse : ServerResponses) {
      SerializedResponses.push_back(ServerResponse.serialize());
    }

    {
      std::unique_lock Lock(ResponseMutex);

      // TODO: Remove debug message
      for (auto &[ServerResponseSize, ServerResponseSerialized] : SerializedResponses) {
        std::cout << "Sent GET response to " << utils::printIPv4(Addr.sin_addr.s_addr) << ":" << ntohs(Addr.sin_port)
                  << "\n";

        ResponseQueue.emplace(Addr, ServerResponseSize, std::move(ServerResponseSerialized));
      }
    }

    break;
  }
  case Request::Method::Put: {
    std::cout << "Received PUT request from " << utils::printIPv4(Addr.sin_addr.s_addr) << ":" << ntohs(Addr.sin_port)
              << "\n";

    auto UserRequest = PutRequest::deserialize(Data.get());
    auto ServerResponse = handlePutRequest(std::move(*UserRequest));
    auto [ServerResponseSize, ServerResponseSerialized] = ServerResponse.serialize();

    {
      std::unique_lock Lock(ResponseMutex);

      std::cout << "Sent PUT response to " << utils::printIPv4(Addr.sin_addr.s_addr) << ":" << ntohs(Addr.sin_port)
                << "\n";

      ResponseQueue.emplace(Addr, ServerResponseSize, std::move(ServerResponseSerialized));
    }

    break;
  }
  case Request::Method::Delete: {
    std::cout << "Received DELETE request from " << utils::printIPv4(Addr.sin_addr.s_addr) << ":"
              << ntohs(Addr.sin_port) << "\n";

    auto UserRequest = DeleteRequest::deserialize(Data.get());
    auto ServerResponse = handleDeleteRequest(std::move(*UserRequest));
    auto [ServerResponseSize, ServerResponseSerialized] = ServerResponse.serialize();

    {
      std::unique_lock Lock(ResponseMutex);

      std::cout << "Sent DELETE response to " << utils::printIPv4(Addr.sin_addr.s_addr) << ":" << ntohs(Addr.sin_port)
                << "\n";

      ResponseQueue.emplace(Addr, ServerResponseSize, std::move(ServerResponseSerialized));
    }

    break;
  }
  default:
    std::cerr << "Not a message";
    return;
  }
}

std::vector<GetResponse> Handler::handleGetRequest(GetRequest &&UserRequest) noexcept {
  // TODO: Check paths
  std::filesystem::path ServerRoot(RootPath), ServerFilePath(UserRequest.Filename);
  std::filesystem::path FilePath = ServerRoot / ServerFilePath.relative_path();
  int Fd = open(FilePath.c_str(), O_RDONLY);

  if (Fd == -1) {
    // TODO: Handle more errors
    std::vector<GetResponse> Responses;
    Responses.emplace_back(Response::Code::FileNotFound, 0, 0, 0, nullptr);

    return Responses;
  }

  std::uint64_t FileSize = lseek(Fd, 0, SEEK_END);
  lseek(Fd, 0, SEEK_SET);

  const std::uint16_t BUFFER_SIZE = 4096;

  std::uint64_t ChunksNumber = (FileSize - 1) / BUFFER_SIZE + 1;

  std::vector<GetResponse> Responses;

  std::uint64_t Offset = 0;

  while (true) {
    auto Buffer = std::make_unique<std::byte[]>(BUFFER_SIZE);
    auto BytesRead = read(Fd, Buffer.get(), BUFFER_SIZE);

    if (BytesRead == 0)
      break;

    Responses.emplace_back(Response::Code::Success, ChunksNumber, Offset, BytesRead, std::move(Buffer));

    Offset += BytesRead;
  }

  close(Fd);

  return Responses;
}

PutResponse Handler::handlePutRequest(PutRequest &&UserRequest) noexcept {
  // TODO: Check paths
  std::filesystem::path ServerRoot(RootPath), ServerFilePath(UserRequest.Filename);
  std::filesystem::path FilePath = ServerRoot / ServerFilePath.relative_path();

  // TODO: Check for errors
  int Fd = open(FilePath.c_str(), O_CREAT | O_WRONLY, 00755);
  lseek(Fd, UserRequest.Offset, SEEK_SET);

  write(Fd, UserRequest.Data.get(), UserRequest.Length);

  return PutResponse(Response::Code::Success, UserRequest.Offset, UserRequest.Length);
}

DeleteResponse Handler::handleDeleteRequest(DeleteRequest &&UserRequest) noexcept {
  // TODO: Check paths
  std::filesystem::path ServerRoot(RootPath), ServerFilePath(UserRequest.Filename);
  std::filesystem::path FilePath = ServerRoot / ServerFilePath.relative_path();

  if (unlink(FilePath.c_str()) < 0) {
    if (errno == ENOENT)
      return DeleteResponse(Response::Code::FileNotFound);
    else
      return DeleteResponse(Response::Code::Unknown);
  }

  return DeleteResponse(Response::Code::Success);
}
} // namespace sockets
